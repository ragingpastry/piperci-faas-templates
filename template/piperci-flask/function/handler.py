from flask import Request
from piperci.faas.this_task import ThisTask


def handle(request: Request, task: ThisTask, config: dict):
    """handle a request to the function
    Args:
        req (str): request body
    """
    return request.get_json(), 200


def validate(request: Request, task: ThisTask, config: dict):
    assert True
