import inspect
import itertools
import os
import tempfile
import traceback

from flask import Flask, Request, request
from piperci.faas.exceptions import PiperError
from piperci.faas.this_task import ThisTask
from typing import Iterable
from werkzeug.exceptions import BadRequest

from .config import Config
from . import handler

app = Flask(__name__)


class Success:
    pass


@app.before_request
def fix_transfer_encoding():
    """
    Sets the "wsgi.input_terminated" environment flag, thus enabling
    Werkzeug to pass chunked requests as streams.  The gunicorn server
    should set this, but it's not yet been implemented.
    """
    transfer_encoding = request.headers.get("Transfer-Encoding", None)
    if transfer_encoding == "chunked":
        request.environ["wsgi.input_terminated"] = True


@app.route("/", defaults={"path": ""}, methods=["POST"])
@app.route("/<path:path>", methods=["POST", "GET"])
def main_route(path):
    Config["path"] = path
    Config["endpoint"] = request.headers.get("X-Forwarded-Host")

    with tempfile.TemporaryDirectory() as tempdir:
        os.chdir(tempdir)
        try:
            json = request.get_json(force=True)
        except BadRequest:
            return f"JSON must be supplied in request", 422

        try:
            data = {
                "storage": {
                    "storage_type": "minio",
                    "access_key": Config["storage"]["access_key"],
                    "secret_key": Config["storage"]["secret_key"],
                    "hostname": Config["storage"]["url"],
                },
                "caller": Config["name"],
                "gman_url": Config["gman"]["url"],
                "status": "started" if Config["type"] == "gateway" else "received",
                "thread_id": json.get("thread_id"),
                "parent_id": json.get("parent_id"),
                "project": json["project"],
                "run_id": json["run_id"],
                "stage": json["stage"],
            }
            task = ThisTask(**data)
        except PiperError as e:
            return f"There was an error creating task {e}", 422
        except KeyError:
            message = traceback.format_exc()
            return f"Invalid JSON detected. {message}", 422

        validation_results = run_common_validations(
            config=Config, request=request, task=task
        )
        handler_validation_results = run_external_validations(
            config=Config, request=request, task=task
        )
        combined_validation_results = itertools.chain(
            validation_results, handler_validation_results
        )

        results_parent_classes = itertools.chain(
            *map(inspect.getmro, combined_validation_results)
        )
        if Exception in results_parent_classes:
            message = f"One or more exceptions found: {validation_results}"
            task.fail(message)
            return message, 400

        try:
            return handler.handle(request=request, task=task, config=Config)
        except Exception:
            error = traceback.format_exc()
            message = f"Unknown error processing function. \n{error}"
            task.fail(message)
            return message, 422


def run_external_validations(request: Request, task: ThisTask, config: dict) -> list:
    """
    Tries to run handler.validate. Expects a method prototype of
    handler.validate(request: flask.Request, config: dict, **kwargs)

    If handler.validate does not exist, an AttributeError will be caught and an
    empty list will be returned.

    :param request: request body
    :param task: ThisTask instance for communication with gman.
    :param config: Configuration
    :return: list of results states
    """
    try:
        handler_validation_results = handler.validate(
            request=request, task=task, config=config
        )
        # handler_validation_results = handler.validate()
        if not handler_validation_results:
            handler_validation_results = [Success]
        task.info("External validation has finished run without issue")
    except AttributeError:
        handler_validation_results = list()
    except Exception as e:
        handler_validation_results = [Exception]
        task.info(
            f"General exception {e} encountered while running external validation"
        )

    return handler_validation_results


def run_common_validations(config: dict, request: Request, task: ThisTask) -> list:
    """
    Runs all common validations, i.e., checking required fields are in requests.
    These should all run, even in case of exceptions, so that you have a list
    of exceptions instead of just failing on the first and returning
    :param config:
    :param request:
    :param task:
    :return: a list of Successes or Errors
    """
    validation_results = list()

    try:
        expected_fields = config["expected-fields"]
    except KeyError:
        expected_fields = list()

    try:
        request_json = request.get_json()
        validate_required_fields_in_request(
            request_json=request_json, expected_fields=expected_fields
        )
        validation_results.append(Success)
    except AssertionError:
        task.info(
            f"Request json does not have a required field in "
            + f"({expected_fields}). Expecting task failure"
        )
        validation_results.append(AssertionError)
    return validation_results


def validate_required_fields_in_request(request_json: dict, expected_fields: Iterable):
    """
    Validates that all required fields are in the request_json.
    :param request_json:
    :param expected_fields:
    :return:
    """
    request_json_keys = set(request_json.keys())
    assert request_json_keys.intersection(set(expected_fields)) == set(expected_fields)
