# 1.0.0 (2019-09-11)


### Bug Fixes

* rename request field to requests, which broke downstream builds ([6e97791](https://gitlab.com/ragingpastry/piperci-faas-templates/commit/6e97791))


### Features

* Provide common interface for both internal and external validation ([bef2c28](https://gitlab.com/ragingpastry/piperci-faas-templates/commit/bef2c28)), closes [#5](https://gitlab.com/ragingpastry/piperci-faas-templates/issues/5)
