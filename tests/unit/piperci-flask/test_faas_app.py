import pytest
import responses

from piperci.faas.exceptions import PiperError
from template.piperci_flask.function import faas_app


@responses.activate
def test_valid_request_passed_to_handler(
    client,
    config,
    mocker,
    monkeypatch,
    request_ctx,
    single_instance,
    start_task_response,
    task_response,
):
    monkeypatch.setattr(faas_app, "Config", config)
    mocker.patch("piperci.faas.this_task.ThisTask", autospec=True)
    config["endpoint"] = request_ctx.request.host_url

    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_response[0], json=task_response[1])

    resp = client.get(
        "test",
        json=single_instance,
        headers={
            "Transfer-Encoding": "chunked",
            "X-Forwarded-Host": config["endpoint"],
        },
    )
    assert resp.status_code == 200


@responses.activate
def test_handler_doesnt_have_validate_still_works(
    client,
    config,
    mocker,
    monkeypatch,
    request_ctx,
    single_instance,
    start_task_response,
):
    monkeypatch.setattr(faas_app, "Config", config)
    mocker.patch("piperci.faas.this_task.ThisTask", autospec=True)
    mocker.patch(
        "template.piperci_flask.function.handler.validate", side_effect=AttributeError
    )
    config["endpoint"] = request_ctx.request.host_url

    responses.add(*start_task_response[0], json=start_task_response[1])

    resp = client.get(
        "test",
        json=single_instance,
        headers={
            "Transfer-Encoding": "chunked",
            "X-Forwarded-Host": config["endpoint"],
        },
    )
    assert resp.status_code == 200


@responses.activate
def test_missing_json_raises_error(
    client, config, mocker, monkeypatch, request_ctx, start_task_response
):
    monkeypatch.setattr(faas_app, "Config", config)
    mocker.patch("piperci.faas.this_task.ThisTask", autospec=True)
    config["endpoint"] = request_ctx.request.host_url

    responses.add(*start_task_response[0], json=start_task_response[1])

    resp = client.get("test", headers={"X-Forwarded-Host": config["endpoint"]})
    assert resp.status_code == 422


def test_piperci_task_creation_fail_raises_error(
    client, config, mocker, monkeypatch, request_ctx, single_instance
):
    monkeypatch.setattr(faas_app, "Config", config)
    mocker.patch(
        "template.piperci_flask.function.faas_app.ThisTask",
        autospec=True,
        side_effect=PiperError,
    )
    config["endpoint"] = request_ctx.request.host_url

    resp = client.get(
        "test", json=single_instance, headers={"X-Forwarded-Host": config["endpoint"]}
    )
    assert resp.status_code == 422


def test_required_fields_missing_keyerror_raises_error(
    client, config, monkeypatch, request_ctx, single_instance
):
    monkeypatch.setattr(faas_app, "Config", config)
    config["endpoint"] = request_ctx.request.host_url
    del single_instance["run_id"]

    resp = client.get(
        "test", json=single_instance, headers={"X-Forwarded-Host": config["endpoint"]}
    )
    assert resp.status_code == 422


@responses.activate
def test_handler_exception_raises_error(
    client,
    config,
    mocker,
    monkeypatch,
    request_ctx,
    single_instance,
    start_task_response,
    task_response,
):
    monkeypatch.setattr(faas_app, "Config", config)
    mocker.patch("piperci.faas.this_task.ThisTask", autospec=True)
    mocker.patch("template.piperci_flask.function.faas_app.ThisTask.fail")
    config["endpoint"] = request_ctx.request.host_url

    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_response[0], json=task_response[1])
    mocker.patch(
        "template.piperci_flask.function.handler.handle", side_effect=Exception
    )

    resp = client.get(
        "test", json=single_instance, headers={"X-Forwarded-Host": config["endpoint"]}
    )
    assert resp.status_code == 422


@responses.activate
@pytest.mark.parametrize(
    "expected_fields",
    [
        {"iterable_field_not_found_set"},
        ("iterable_field_not_found_tuple",),
        ["iterable_field_not_found_list"],
    ],
)
def test_required_field_not_found(
    client,
    config,
    expected_fields,
    mocker,
    monkeypatch,
    request_ctx,
    single_instance,
    start_task_response,
    task_response,
):
    config["expected-fields"] = expected_fields
    monkeypatch.setattr(faas_app, "Config", config)
    mocker.patch("piperci.faas.this_task.ThisTask", autospec=True)
    mocker.patch("template.piperci_flask.function.faas_app.ThisTask.fail")
    config["endpoint"] = request_ctx.request.host_url

    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_response[0], json=task_response[1])

    resp = client.get(
        "test",
        json=single_instance,
        headers={
            "Transfer-Encoding": "chunked",
            "X-Forwarded-Host": config["endpoint"],
        },
    )
    assert resp.status_code == 400


@responses.activate
def test_external_validation_general_exception(
    client,
    config,
    mocker,
    monkeypatch,
    request_ctx,
    single_instance,
    start_task_response,
    task_response,
):
    monkeypatch.setattr(faas_app, "Config", config)
    mocker.patch("piperci.faas.this_task.ThisTask", autospec=True)
    mocker.patch(
        "template.piperci_flask.function.handler.validate", side_effect=Exception
    )
    mocker.patch("template.piperci_flask.function.faas_app.ThisTask.fail")
    config["endpoint"] = request_ctx.request.host_url

    responses.add(*start_task_response[0], json=start_task_response[1])
    responses.add(*task_response[0], json=task_response[1])

    resp = client.get(
        "test",
        json=single_instance,
        headers={
            "Transfer-Encoding": "chunked",
            "X-Forwarded-Host": config["endpoint"],
        },
    )
    assert resp.status_code == 400
