import os
import pytest
import responses
import sys

from typing import Tuple


try:
    sys.path.insert(0, os.environ["MODULE_ROOT"])
except KeyError:
    pass

from template.piperci_flask.function import faas_app


@pytest.fixture
def app():
    return faas_app.app


@pytest.fixture
def config():
    cfg = {
        "gman": {"url": "http://172.17.0.1:8089"},
        "storage": {
            "url": "172.17.0.1:9000",
            "access_key_secret": "/var/openfaas/secrets/access-key",
            "secret_key_secret": "/var/openfaas/secrets/secret-key",
            "access_key": "",
            "secret_key": "",
        },
        "name": "piperci-flask",
        "executor_url": "http://172.17.0.1:8080/async-function/piperci-flask-executor",
        "type": "gateway",
    }
    return cfg


@pytest.fixture
def single_instance():
    return {"project": "A Test Project", "run_id": 1, "stage": "tox_tests"}


@pytest.fixture
def start_task_response(config) -> Tuple[Tuple, dict]:
    return (
        (responses.POST, f"{config['gman']['url']}/task"),
        {
            "task": {
                "parent_id": None,
                "run_id": "create_1",
                "project": "gman_test_data",
                "thread_id": "9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
                "caller": "test_case_create_1",
                "task_id": "9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
            }
        },
    )


@pytest.fixture
def task_response(config) -> Tuple[Tuple, dict]:
    return (
        (
            responses.PUT,
            f"{config['gman']['url']}/task/9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
        ),
        {
            "timestamp": "2019-07-17T12:10:32.952267+00:00",
            "event_id": "9038675b-66ad-4d8f-9c38-a3fa5a93db8c",
            "task": {
                "parent_id": None,
                "run_id": "create_1",
                "project": "gman_test_data",
                "thread_id": "9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
                "caller": "test_case_create_1",
                "task_id": "9c4d3068-fa2d-4cdf-ae10-731c82cd7d2d",
            },
            "return_code": None,
            "status": "started",
            "message": " task creation body",
        },
    )
